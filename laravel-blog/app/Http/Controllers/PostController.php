<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// access to the authenticated user via the Auth Facade
use Illuminate\Support\Facades\Auth;

// Implement the the database manipulation
use App\Models\Post;

class PostController extends Controller
{
    // action to return a view containing a form for a blog post creation.
    public function create(){
        return view('posts.create');
    }

    // action to receive form data and subsequently store said data in the posts table.
    // Laravel's "Request" class contains the data from the form submission
    public function store(Request $request){
        // if there is an authenticated user.
        if(Auth::user()){
            // Instantiate a new Post object from the Post model class.
            $post = new Post;
            // define the properties of the $post object using the received form data.
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            // get the id of the authenticated user and set it as the foreign key value of user_id of the new post.
            $post->user_id = (Auth::user()->id);

            // save this post object in the database
            $post->save();

            return redirect('/posts');
        }
        else{
            return redirect('/login');
        }
    }

    // action that will return a view showing all blog posts
    // public function index(){
    //     // Fetches all records from the table without any specific condition.
    //     // $posts = Post::all();
    //     // Fetches all records from the table with specific conditions (if need to build more complex queries).
    //     $posts = Post::get();

    //     // with() method is used to pass data to views file.
    //     // this assigns a variable name ('posts') and its corresponding data ($posts collection) to views.
    //     return view('posts.index')->with('posts', $posts);
    // }

    // // s02 Activity
    // // action that will return a view showing three random blog post
    // public function welcome(){
        
    //     $posts = Post::inRandomOrder()
    //     ->limit(3)
    //     ->get();

    //     return view('welcome')->with('posts', $posts);
    // }

    // action for showing only the posts that is authored by the authenticated user.
    public function myPosts(){
        if(Auth::user()){
            // retrieve the posts authored by the current logged in user.
            // If you have sucessfully established the model relationship, you can access related data.
            $posts = Auth::user()->posts;

            return view('posts.index')->with('posts', $posts);
        }
        else{
            return redirect('/login');
        }
    }

    // action for showing a view of a specific post using the URL parameter 'id' to query for the database entry to be shown.
    // Laravel automatically captures the value from the URL parameter and passes it as an argument to the corresponding controller method.
    public function show($id){
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    //define the edit action in the PostController class to find a post using the id passed in via the URL parameter and pass it to an edit form view
    public function edit($id){
    $post = Post::find($id);
    if (!$post) {
        return redirect()->route('posts.index')->with('error', 'Post not found');
    }
    //edit form should not be accessible if the user doesn’t owned the post
    if (!Auth::check() || Auth::user()->id !== $post->user_id) {
        return redirect()->route('posts.index')->with('error', 'Unauthorized access');
    }
        return view('posts.edit', with('post', $post));
    }

    // public function update(Request $request, $id){
    // $post = Post::find($id);

    // //title and content should not be empty
    // $validatedData = $request->validate([
    //     'title' => 'required',
    //     'content' => 'required',
    // ]);

    // $post->title = $validatedData['title'];
    // $post->content = $validatedData['content'];
    // $post->save();

    //     return redirect()->route('posts.show', $post->id)->with('success', 'Post updated successfully');
    // }

     // action that will overwrite an existing post with mathching $id.
     public function update(Request $request, $id){
        $post = Post::find($id);
        // if authenticated user's ID is the same as the post's user_id
        if(Auth::user()->id == $post->user_id){
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        }
        return redirect('/posts');
    }

    // action that will delete a specific post based on the given id.
    public function destroy($id){
        $post = Post::find($id);

        if(Auth::user()->id == $post->user_id){
            $post->delete();
        }
        return redirect('/posts');
    }

    //action that will archive a post 
    public function archive($id){
        $post = Post::find($id);
        
        if ($post) {
            $post->isActive = false;
            $post->save();
        }
        return redirect('/posts');
    }

    //modify the PostController's index & welcome action to instead return ONLY POST THAT ARE ACTIVE.
    public function index(){
        $posts = Post::where('isActive', true)->get();
        return view('posts.index', ['posts' => $posts]);
    }

    public function welcome(){
        $posts = Post::where('isActive', true)->get();

        $posts = Post::inRandomOrder()
            ->limit(3)
            ->get();
        return view('welcome', ['posts' => $posts]);
    }

}
