@extends('layouts.app')

@section('content')
	{{-- Check if there are any posts to display. --}}
	@if(count($posts) > 0)
		@foreach($posts as $post)
			<div class="card text-center mt-3">
				<div class="card-body">
					<h4 class="card-title mb-3">
						<a href="/posts/{{$post->id}}">{{$post->title}}</a>
					</h4>
					<h6 class="card-text mb-3">
						Author: {{$post->user->name}}
					</h6>
					<p class="card-subtitle mb-3 text-muted">
						Created at: {{$post->created_at}}
					</p>
				</div>
				
			@if(Auth::user())
				{{-- Check if the authenticated user is the author of the blog post --}}
				@if(Auth::user()->id == $post->user_id)
					<div class="card-footer">
						<form method="POST" action="/posts/{{$post->id}}">
							{{-- 
								Method Spoofing
									- HTML do not support PUT or DELETE
									- using the @method() this will add a hidden attribute to specify the HTTP request method
							 --}}
							@method('DELETE')
							@csrf
							<a href="/posts/{{$post->id}}/edit" class="btn btn-primary">
								Edit Post
							</a>
							<button type="submit" class="btn btn-danger">
								Delete Post
							</button>
						</form>
					</div>
				@endif
			@endif
			</div>
		@endforeach
	@else
		<div>
			<h2>There are no posts to show.</h2>
			<a href="/posts/create" class="btn btn-info">Create post</a>
		</div>
	@endif
@endsection
