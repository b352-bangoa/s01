{{-- s02 Activity solution --}}

<!-- @extends('layouts.app')

@section('content')
    <div class="text-center">
        <img class="rounded w-50" src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" alt="Laravel Logo Loading">
    </div>

    <h1 class="text-center mt-3">Featured Posts</h1>

    @php $postCount = 0; @endphp
    @foreach ($random_posts as $post)
        @if ($postCount < 3)
            <div class="card text-center mt-4">
                <div class="card-body">
                    <h4 class="card-title mb-3">
                        <a href="{{ route('posts.show', $post->id) }}">{{ $post->title }}</a>
                    </h4>
                    <h6 class="card-text mb-3">
                        Author: {{ $post->user->name }}
                    </h6>
                    <p class="card-subtitle mb-3 text-muted">
                        Created at: {{ $post->created_at }}
                    </p>
                </div>
            </div>
            @php $postCount++; @endphp
        @else
            @break
        @endif
    @endforeach

    @if ($random_posts->count() > 3)
        <div class="text-center mt-4">
            <a href="{{ route('posts.index') }}" class="btn btn-primary">View More Posts</a>
        </div>
    @endif
@endsection -->

@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>
			<div class="mt-3">
				<a href="/posts" class="card-link">View all posts</a>
			</div>
		</div>
	</div>
@endsection

